using System.Collections;
using UnityEngine;
using UnityEngine.EventSystems;


namespace A2D.Tools
{
    public class GetFocusOnEnable : MonoBehaviour
    {
        protected virtual void OnEnable()
        {
            if (EventSystem.current)
            {
                StartCoroutine("SelectGameObject");
            }
        }

        protected virtual IEnumerator SelectGameObject()
        {
            EventSystem.current.SetSelectedGameObject(null);
            yield return null;
            EventSystem.current.SetSelectedGameObject(this.gameObject);
        }
    }
}