using UnityEngine;


namespace A2D.Tools
{
    public class A2DLayers
    {
        public static bool LayerInLayerMask(int layer, LayerMask layerMask)
        {
            return (((1 << layer) & layerMask) != 0);
        }

    }
}