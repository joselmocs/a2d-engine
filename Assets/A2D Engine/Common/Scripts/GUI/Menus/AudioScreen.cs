using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;


namespace A2D.Engine
{
    public class AudioScreen : MonoBehaviour
    {
        SettingsManager _settingsManager;

        // Sliders
        Slider masterVolumeSlider;
        Slider musicVolumeSlider;
        Slider soundVolumeSlider;

        void Start()
        {
            _settingsManager = SettingsManager.Instance;

            masterVolumeSlider = transform.Find("Options").Find("Master").Find("Slider").Find("Slider").GetComponent<Slider>();
            masterVolumeSlider.value = _settingsManager.gameSettings.masterVolume;
            masterVolumeSlider.onValueChanged.AddListener(delegate { onMasterValueChanged(); });

            musicVolumeSlider = transform.Find("Options").Find("Music").Find("Slider").Find("Slider").GetComponent<Slider>();
            musicVolumeSlider.value = _settingsManager.gameSettings.musicVolume;
            musicVolumeSlider.onValueChanged.AddListener(delegate { onMusicValueChanged(); });

            soundVolumeSlider = transform.Find("Options").Find("Sound").Find("Slider").Find("Slider").GetComponent<Slider>();
            soundVolumeSlider.value = _settingsManager.gameSettings.soundVolume;
            soundVolumeSlider.onValueChanged.AddListener(delegate { onSoundValueChanged(); });

            UpdateView();
        }

        public void onMasterValueChanged()
        {
            _settingsManager.gameSettings.masterVolume = masterVolumeSlider.value;
            _settingsManager.gameSettings.Save();

            UpdateView();
        }

        public void onMusicValueChanged()
        {
            _settingsManager.gameSettings.musicVolume = musicVolumeSlider.value;
            _settingsManager.gameSettings.Save();

            UpdateView();
        }

        public void onSoundValueChanged()
        {
            _settingsManager.gameSettings.soundVolume = soundVolumeSlider.value;
            _settingsManager.gameSettings.Save();

            UpdateView();
        }

        protected virtual void UpdateView()
        {
            transform.Find("Options").Find("Master").Find("Value").Find("Text").GetComponent<Text>().text = _settingsManager.gameSettings.masterVolume.ToString();
            transform.Find("Options").Find("Music").Find("Value").Find("Text").GetComponent<Text>().text = _settingsManager.gameSettings.musicVolume.ToString();
            transform.Find("Options").Find("Sound").Find("Value").Find("Text").GetComponent<Text>().text = _settingsManager.gameSettings.soundVolume.ToString();
        }

        public void OnResetOptions()
        {
            masterVolumeSlider.value = 10f;
            musicVolumeSlider.value = 10f;
            soundVolumeSlider.value = 10f;
        }
    }
}