﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class AttachOptionArrow : MonoBehaviour {

    protected bool currentlySelected;
    protected Image LeftArrow;
    protected Image RightArrow;
    public GameObject _gameObject;

    void Awake()
    {
        if (!_gameObject) {
            _gameObject = this.gameObject;
        }

        LeftArrow = _gameObject.transform.Find("Left").transform.Find("Image").GetComponent<Image>();
        RightArrow = _gameObject.transform.Find("Right").transform.Find("Image").GetComponent<Image>();
    }

    void Start()
    {
        LeftArrow.enabled = false;
        RightArrow.enabled = false;
    }

    void Update ()
    {
        if (!EventSystem.current) {
            return;
        }

        if (currentlySelected) {
            if (EventSystem.current.currentSelectedGameObject != this.gameObject) {
                currentlySelected = false;

                LeftArrow.enabled = false;
                RightArrow.enabled = false;
            }

            return;
        }

        if (EventSystem.current.currentSelectedGameObject == this.gameObject) {
            currentlySelected = true;

            LeftArrow.enabled = true;
            RightArrow.enabled = true;
        }
    }
}
