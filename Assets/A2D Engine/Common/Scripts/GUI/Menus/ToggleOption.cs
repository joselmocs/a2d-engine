﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using A2D.Tools;


namespace A2D.Engine
{
    public class ToggleOption : MonoBehaviour
    {
        public UnityEvent OnLeftHorizontalInput;
        public UnityEvent OnRightHorizontalInput;

        protected float changeCooldown = 0.3f;
        protected float _cooldownTimeStamp = 0;

        void Update()
        {
            if (_cooldownTimeStamp > Time.unscaledTime) {
                return;
            }

            if (!EventSystem.current) {
                return;
            }

            if (EventSystem.current.currentSelectedGameObject != this.gameObject) {
                return;
            }

            if (Input.GetAxisRaw("Horizontal") == -1f) {
                _cooldownTimeStamp = Time.unscaledTime + changeCooldown;
                OnLeftHorizontalInput.Invoke();
            }
            else if (Input.GetAxisRaw("Horizontal") == 1f) {
                _cooldownTimeStamp = Time.unscaledTime + changeCooldown;
                OnRightHorizontalInput.Invoke();
            }
        }
    }
}
