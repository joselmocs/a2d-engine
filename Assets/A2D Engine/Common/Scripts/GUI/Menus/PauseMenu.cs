using System.Collections.Generic;
using UnityEngine;
using A2D.Tools;


namespace A2D.Engine
{
    public class PauseMenu : MonoBehaviour
    {
        InputManager _inputManager;

        GameObject PauseScreen;
        GameObject QuitScreen;
        Animator _pauseOverlay;
        GameObject _currentScreen;

        void Awake()
        {
            PauseScreen = transform.Find("PauseScreen").gameObject;
            QuitScreen = transform.Find("QuitScreen").gameObject;

            Transform _overlayTransform = transform.parent.Find("PauseOverlay");
            if (_overlayTransform) {
                _pauseOverlay = _overlayTransform.GetComponent<Animator>();;
            }
        }

        void Start()
        {
            _inputManager = InputManager.Instance;
        }

        void OnEnable()
        {
            if (_pauseOverlay) {
                _pauseOverlay.SetBool("ShowOverlay", true);
            }

            SetCurrentScreen(PauseScreen);
        }

        void Update()
        {
            if (_currentScreen == PauseScreen) {
                HandlePauseScreen();
            }
            else if (_currentScreen == QuitScreen) {
                HandleQuitScreen();
            }
        }

        public void SetCurrentScreen(GameObject gameObject)
        {
            if (_currentScreen) {
                _currentScreen.SetActive(false);
            }

            if (gameObject) {
                gameObject.SetActive(true);
                _currentScreen = gameObject;
            }
        }

        void HandlePauseScreen()
        {
            if (_inputManager.B_Button.State.CurrentState == A2DInput.ButtonStates.ButtonDown ||
                _inputManager.Start_Button.State.CurrentState == A2DInput.ButtonStates.ButtonDown)
            {
                UnPause();
            }
        }

        void HandleQuitScreen()
        {
            if (_inputManager.B_Button.State.CurrentState == A2DInput.ButtonStates.ButtonDown)
            {
                SetCurrentScreen(PauseScreen);
            }
        }

        public void UnPause()
        {
            if (_pauseOverlay) {
                _pauseOverlay.SetBool("ShowOverlay", false);
            }

            A2DEventManager.TriggerEvent(new A2DEngineEvent(A2DEngineEventTypes.UnPause));
        }

        public void QuitApplication()
        {
            A2DEventManager.TriggerEvent(new A2DEngineEvent(A2DEngineEventTypes.Quit));
        }
    }
}