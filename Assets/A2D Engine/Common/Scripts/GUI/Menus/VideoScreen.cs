using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;


namespace A2D.Engine
{
    public class VideoScreen : MonoBehaviour
    {
        SettingsManager _settingsManager;

        // Resolution
        List<Resolution> resolutionsList;
        Text ResolutionToggleText;
        int currentResolutionIndex;

        // FullScreen
        private Text FullScreenToggleText;

        // Quality
        Text QualityToggleText;
        int currentQualityLevelIndex;

        void Awake()
        {
            _settingsManager = SettingsManager.Instance;

            resolutionsList = new List<Resolution>();

            for (int i = 0; i < Screen.resolutions.Length; i++) {
                if (Screen.resolutions[i].refreshRate == Screen.currentResolution.refreshRate) {
                    resolutionsList.Add(Screen.resolutions[i]);
                }
            }
        }

        void Start()
        {
            ResolutionToggleText = this.transform.Find("Options").Find("Resolution").Find("Value").Find("Text").GetComponent<Text>();
            FullScreenToggleText = this.transform.Find("Options").Find("Fullscreen").Find("Value").Find("Text").GetComponent<Text>();
            QualityToggleText = this.transform.Find("Options").Find("Quality").Find("Value").Find("Text").GetComponent<Text>();

            for (int i = 0; i < resolutionsList.Count; i++)
            {
                if (resolutionsList[i].width == _settingsManager.gameSettings.resolutionWidth) {
                    currentResolutionIndex = i;
                }
            }

            currentQualityLevelIndex = _settingsManager.gameSettings.qualityLevel;

            UpdateView();
        }

        public virtual void UpdateView()
        {
            ResolutionToggleText.text = resolutionsList[currentResolutionIndex].ToString();
            FullScreenToggleText.text = (_settingsManager.gameSettings.fullScreen) ? "ON" : "OFF";
            QualityToggleText.text = QualitySettings.names[_settingsManager.gameSettings.qualityLevel];
        }

        public virtual void ToggleFullScreen()
        {
            _settingsManager.gameSettings.fullScreen = !_settingsManager.gameSettings.fullScreen;
            _settingsManager.gameSettings.Save();

            UpdateView();
        }

        public virtual void SetLessResolution()
        {
            currentResolutionIndex -= 1;
            if (currentResolutionIndex < 0) {
                currentResolutionIndex = resolutionsList.Count - 1;
            }

            SetResolution(currentResolutionIndex);
        }

        public virtual void SetMoreResolution()
        {
            currentResolutionIndex += 1;
            if (currentResolutionIndex == resolutionsList.Count) {
                currentResolutionIndex = 0;
            }

            SetResolution(currentResolutionIndex);
        }

        void SetResolution(int resolutionIndex)
        {
            _settingsManager.gameSettings.resolutionWidth = resolutionsList[resolutionIndex].width;
            _settingsManager.gameSettings.resolutionHeight = resolutionsList[resolutionIndex].height;
            _settingsManager.gameSettings.Save();

            UpdateView();
        }

        public virtual void SetLessQualityLevel()
        {
            currentQualityLevelIndex -= 1;
            if (currentQualityLevelIndex < 0) {
                currentQualityLevelIndex = QualitySettings.names.Length - 1;
            }

            _settingsManager.gameSettings.qualityLevel = currentQualityLevelIndex;
            _settingsManager.gameSettings.Save();

            UpdateView();
        }

        public virtual void SetMoreQualityLevel()
        {
            currentQualityLevelIndex += 1;
            if (currentQualityLevelIndex == QualitySettings.names.Length) {
                currentQualityLevelIndex = 0;
            }

            _settingsManager.gameSettings.qualityLevel = currentQualityLevelIndex;
            _settingsManager.gameSettings.Save();

            UpdateView();
        }

        public void OnResetOptions()
        {
            _settingsManager.gameSettings.fullScreen = true;

            currentQualityLevelIndex = QualitySettings.names.Length - 1;
            _settingsManager.gameSettings.qualityLevel = currentQualityLevelIndex;

            currentResolutionIndex = resolutionsList.Count - 1;
            _settingsManager.gameSettings.resolutionWidth = resolutionsList[currentResolutionIndex].width;
            _settingsManager.gameSettings.resolutionHeight = resolutionsList[currentResolutionIndex].height;
            _settingsManager.gameSettings.Save();

            UpdateView();
        }
    }
}