﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using A2D.Tools;


namespace A2D.Engine
{
    public class OptionsMenu : MonoBehaviour
    {
        InputManager _inputManager;

        GameObject OptionsScreen;
        GameObject AudioScreen;
        GameObject VideoScreen;
        GameObject _currentScreen;

        void Awake()
        {
            OptionsScreen = transform.Find("OptionsScreen").gameObject;
            AudioScreen = transform.Find("AudioScreen").gameObject;
            VideoScreen = transform.Find("VideoScreen").gameObject;
        }

        void Start()
        {
            _inputManager = InputManager.Instance;
        }

        void OnEnable()
        {
            SetCurrentScreen(OptionsScreen);
        }

        void Update()
        {
            if (_currentScreen == OptionsScreen) {
                HandleOptionsScreen();
            }
            else if (_currentScreen == AudioScreen) {
                HandleAudioScreen();
            }
            else if (_currentScreen == VideoScreen) {
                HandleVideoScreen();
            }
        }

        public void SetCurrentScreen(GameObject gameObject)
        {
            if (_currentScreen) {
                _currentScreen.SetActive(false);
            }

            if (gameObject) {
                gameObject.SetActive(true);
                _currentScreen = gameObject;
            }
        }

        protected virtual void HandleOptionsScreen()
        {
            if (_inputManager.B_Button.State.CurrentState == A2DInput.ButtonStates.ButtonDown)
            {
                OnOptionsClose();
            }
        }

        protected virtual void HandleAudioScreen()
        {
            if (_inputManager.B_Button.State.CurrentState == A2DInput.ButtonStates.ButtonDown)
            {
                SetCurrentScreen(OptionsScreen);
            }
        }

        protected virtual void HandleVideoScreen()
        {
            if (_inputManager.B_Button.State.CurrentState == A2DInput.ButtonStates.ButtonDown)
            {
                SetCurrentScreen(OptionsScreen);
            }
        }

        public void OnOptionsClose()
        {
            SetCurrentScreen(null);
            this.gameObject.SetActive(false);

            if (GameManager.Instance.Paused) {
                GUIManager.Instance.PauseMenu.SetActive(true);
            }
        }
    }
}