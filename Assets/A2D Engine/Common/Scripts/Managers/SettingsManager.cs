﻿using UnityEngine;
using A2D.Tools;


namespace A2D.Engine
{
    public class SettingsManager : Singleton<SettingsManager>
    {
        public GameSettings gameSettings;

        protected override void Awake()
        {
            base.Awake();

            gameSettings = new GameSettings(Application.persistentDataPath + "/settings.json");
            gameSettings.Load();
        }
    }
}
