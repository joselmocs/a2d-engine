using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using A2D.Tools;


namespace A2D.Engine
{
    /// <summary>
    /// This persistent singleton handles the inputs and sends commands to the player.
    /// IMPORTANT : this script's Execution Order MUST be -100.
    /// You can define a script's execution order by clicking on the script's file and then clicking on the Execution Order button at the bottom right of the script's inspector.
    /// See https://docs.unity3d.com/Manual/class-ScriptExecution.html for more details
    /// </summary>
    [AddComponentMenu("Adventure2D/Managers/Input Manager")]
    public class InputManager : Singleton<InputManager>
    {
        /// the possible kinds of control used for movement
        [Header("Movement settings")]
        [Information("Turn SmoothMovement on to have inertia in your controls (meaning there'll be a small delay between a press/release of a direction and your character moving/stopping).", InformationAttribute.InformationType.Info, false)]
        /// If set to true, acceleration / deceleration will take place when moving / stopping
        public bool SmoothMovement = true;
        /// the minimum horizontal and vertical value you need to reach to trigger movement on an analog controller (joystick for example)
        [Information("The minimum horizontal and vertical value you need to reach to trigger movement on an analog controller (joystick for example).", InformationAttribute.InformationType.Info, false)]
        public Vector2 Threshold = new Vector2(0.1f, 0.4f);

        /// the start button
        public A2DInput.IButton Start_Button { get; protected set; }

        /// the A button, used for jumps and validation
        public A2DInput.IButton A_Button { get; protected set; }

        /// the B button, used for cancel
        public A2DInput.IButton B_Button { get; protected set; }

        /// the primary movement value (used to move the character around)
        public Vector2 PrimaryMovement {
            get {
                return _primaryMovement;
            }
        }
        /// the secondary movement (usually the right stick on a gamepad), used to aim
        protected List<A2DInput.IButton> ButtonList;
        protected Vector2 _primaryMovement = Vector2.zero;
        protected string _axisHorizontal;
        protected string _axisVertical;

        /// <summary>
        /// On Start we look for what mode to use, and initialize our axis and buttons
        /// </summary>
        protected virtual void Start()
        {
            InitializeButtons();
            InitializeAxis();
        }

        /// <summary>
        /// Initializes the buttons. If you want to add more buttons, make sure to register them here.
        /// </summary>
        protected virtual void InitializeButtons()
        {
            ButtonList = new List<A2DInput.IButton>();
            ButtonList.Add(Start_Button = new A2DInput.IButton("Start", Start_ButtonDown, Start_ButtonPressed, Start_ButtonUp));
            ButtonList.Add(A_Button = new A2DInput.IButton("A", A_ButtonDown, A_ButtonPressed, A_ButtonUp));
            ButtonList.Add(B_Button = new A2DInput.IButton("B", B_ButtonDown, B_ButtonPressed, B_ButtonUp));
        }

        /// <summary>
        /// Initializes the axis strings.
        /// </summary>
        protected virtual void InitializeAxis()
        {
            _axisHorizontal = "Horizontal";
            _axisVertical = "Vertical";
        }

        /// <summary>
        /// On LateUpdate, we process our button states
        /// </summary>
        protected virtual void LateUpdate()
        {
            ProcessButtonStates();
        }

        /// <summary>
        /// At update, we check the various commands and update our values and states accordingly.
        /// </summary>
        protected virtual void Update()
        {
            SetMovement();
            GetInputButtons();
        }

        /// <summary>
        /// If we're not on mobile, watches for input changes, and updates our buttons states accordingly
        /// </summary>
        protected virtual void GetInputButtons()
        {
            foreach (A2DInput.IButton button in ButtonList)
            {
                if (Input.GetButton(button.ButtonID))
                {
                    button.TriggerButtonPressed();
                }
                if (Input.GetButtonDown(button.ButtonID))
                {
                    button.TriggerButtonDown();
                }
                if (Input.GetButtonUp(button.ButtonID))
                {
                    button.TriggerButtonUp();
                }
            }
        }

        /// <summary>
        /// Called at LateUpdate(), this method processes the button states of all registered buttons
        /// </summary>
        public virtual void ProcessButtonStates()
        {
            // for each button, if we were at ButtonDown this frame, we go to ButtonPressed. If we were at ButtonUp, we're now Off
            foreach (A2DInput.IButton button in ButtonList)
            {
                if (button.State.CurrentState == A2DInput.ButtonStates.ButtonDown)
                {
                    button.State.ChangeState(A2DInput.ButtonStates.ButtonPressed);
                }
                if (button.State.CurrentState == A2DInput.ButtonStates.ButtonUp)
                {
                    button.State.ChangeState(A2DInput.ButtonStates.Off);
                }
            }
        }

        /// <summary>
        /// Called every frame, gets primary movement values from input
        /// </summary>
        public virtual void SetMovement()
        {
            if (SmoothMovement)
            {
                _primaryMovement.x = Input.GetAxis(_axisHorizontal);
                _primaryMovement.y = Input.GetAxis(_axisVertical);
            }
            else
            {
                _primaryMovement.x = Input.GetAxisRaw(_axisHorizontal);
                _primaryMovement.y = Input.GetAxisRaw(_axisVertical);
            }
        }

        /// Start button
        public virtual void Start_ButtonDown() { Start_Button.State.ChangeState(A2DInput.ButtonStates.ButtonDown); }
        public virtual void Start_ButtonPressed() { Start_Button.State.ChangeState(A2DInput.ButtonStates.ButtonPressed); }
        public virtual void Start_ButtonUp() { Start_Button.State.ChangeState(A2DInput.ButtonStates.ButtonUp); }

        /// A button
        public virtual void A_ButtonDown() { A_Button.State.ChangeState(A2DInput.ButtonStates.ButtonDown); }
        public virtual void A_ButtonPressed() { A_Button.State.ChangeState(A2DInput.ButtonStates.ButtonPressed); }
        public virtual void A_ButtonUp() { A_Button.State.ChangeState(A2DInput.ButtonStates.ButtonUp); }

        /// B button
        public virtual void B_ButtonDown() { B_Button.State.ChangeState(A2DInput.ButtonStates.ButtonDown); }
        public virtual void B_ButtonPressed() { B_Button.State.ChangeState(A2DInput.ButtonStates.ButtonPressed); }
        public virtual void B_ButtonUp() { B_Button.State.ChangeState(A2DInput.ButtonStates.ButtonUp); }
    }
}