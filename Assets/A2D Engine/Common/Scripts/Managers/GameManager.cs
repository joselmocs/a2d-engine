using System.Collections.Generic;
using UnityEngine;
using A2D.Tools;


namespace A2D.Engine
{
    /// <summary>
    /// A list of the possible Adventure Engine base events
    /// </summary>
    public enum A2DEngineEventTypes
    {
        Pause,
        UnPause,
        Quit,
    }

    /// <summary>
    /// A type of events used to signal level start and end (for now)
    /// </summary>
    public struct A2DEngineEvent
    {
        public A2DEngineEventTypes EventType;
        /// <summary>
        /// Initializes a new instance of the <see cref="A2D.Engine.A2DEngineEvent"/> struct.
        /// </summary>
        /// <param name="eventType">Event type.</param>
        public A2DEngineEvent(A2DEngineEventTypes eventType)
        {
            EventType = eventType;
        }
    }

    /// <summary>
    /// The game manager is a persistent singleton that handles points and time
    /// </summary>
    [AddComponentMenu("Adventure2D/Managers/Game Manager")]
    public class GameManager : PersistentSingleton<GameManager>,
                               A2DEventListener<A2DEngineEvent>
    {
        [Header("Settings")]
        /// the target frame rate for the game
        public int TargetFrameRate = 300;


        /// true if the game is currently paused
        public bool Paused { get; set; }

        // storage
        protected Stack<float> _savedTimeScale;

        /// <summary>
        /// Initialization
        /// </summary>
        protected override void Awake()
        {
            base.Awake();
        }

        /// <summary>
        /// On Start(), sets the target framerate to whatever's been specified
        /// </summary>
        protected virtual void Start()
        {
            Application.targetFrameRate = TargetFrameRate;
            _savedTimeScale = new Stack<float>();
        }

        /// <summary>
        /// this method resets the whole game manager
        /// </summary>
        public virtual void Reset()
        {
            Time.timeScale = 1f;
            Paused = false;
        }

        /// <summary>
        /// sets the timescale to the one in parameters
        /// </summary>
        /// <param name="newTimeScale">New time scale.</param>
        public virtual void SetTimeScale(float newTimeScale)
        {
            _savedTimeScale.Push(Time.timeScale);
            Time.timeScale = newTimeScale;
        }

        /// <summary>
        /// Resets the time scale to the last saved time scale.
        /// </summary>
        public virtual void ResetTimeScale()
        {
            if (_savedTimeScale.Count > 0)
            {
                Time.timeScale = _savedTimeScale.Peek();
                _savedTimeScale.Pop();
            }
            else
            {
                Time.timeScale = 1f;
            }
        }

        /// <summary>
        /// Pauses the game or unpauses it depending on the current state
        /// </summary>
        public virtual void Pause()
        {
            Instance.SetTimeScale(0.0f);
            Instance.Paused = true;

            if (GUIManager.Instance != null)
            {
                GUIManager.Instance.SetPause(true);
            }
        }

        /// <summary>
        /// Unpauses the game
        /// </summary>
        public virtual void UnPause()
        {
            Instance.ResetTimeScale();
            Instance.Paused = false;

            if (GUIManager.Instance != null)
            {
                GUIManager.Instance.SetPause(false);
            }
        }

        /// <summary>
        /// Exit the game
        /// </summary>
        public virtual void Quit()
        {
#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#else
            Application.Quit();
#endif
        }

        /// <summary>
        /// Catches A2DEngineEvents and acts on them, playing the corresponding sounds
        /// </summary>
        /// <param name="engineEvent">A2DEngineEvent event.</param>
        public virtual void OnA2DEvent(A2DEngineEvent engineEvent)
        {
            switch (engineEvent.EventType)
            {
                case A2DEngineEventTypes.Pause:
                    Pause();
                    break;

                case A2DEngineEventTypes.UnPause:
                    UnPause();
                    break;

                case A2DEngineEventTypes.Quit:
                    Quit();
                    break;
            }
        }

        /// <summary>
        /// OnDisable, we start listening to events.
        /// </summary>
        protected virtual void OnEnable()
        {
            this.A2DEventStartListening<A2DEngineEvent>();
        }

        /// <summary>
        /// OnDisable, we stop listening to events.
        /// </summary>
        protected virtual void OnDisable()
        {
            this.A2DEventStopListening<A2DEngineEvent>();
        }
    }
}