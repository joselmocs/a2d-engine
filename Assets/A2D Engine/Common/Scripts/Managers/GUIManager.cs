using UnityEngine;
using UnityEngine.EventSystems;
using A2D.Tools;


namespace A2D.Engine
{
    /// <summary>
    /// Handles all GUI effects and changes
    /// </summary>
    [AddComponentMenu("Adventure2D/Managers/GUI Manager")]
    public class GUIManager : Singleton<GUIManager>
    {
        /// the game object that contains the heads up display
        public GameObject HUD;
        /// the pause screen game object
        public GameObject PauseMenu;

        InputManager _inputManager;

        protected override void Awake()
        {
            base.Awake();

            _inputManager = InputManager.Instance;
        }

        void Update()
        {
            HandlePause();
        }

        void HandlePause()
        {
            if (GameManager.Instance.Paused) {
                return;
            }

            if (_inputManager.Start_Button.State.CurrentState == A2DInput.ButtonStates.ButtonDown)
            {
                A2DEventManager.TriggerEvent(new A2DEngineEvent(A2DEngineEventTypes.Pause));
            }
        }


        /// <summary>
        /// Sets the HUD active or inactive
        /// </summary>
        /// <param name="state">If set to <c>true</c> turns the HUD active, turns it off otherwise.</param>
        public virtual void SetHUDActive(bool state)
        {
            if (HUD != null)
            {
                HUD.SetActive(state);
            }
        }

        /// <summary>
        /// Sets the pause.
        /// </summary>
        /// <param name="state">If set to <c>true</c>, sets the pause.</param>
        public virtual void SetPause(bool state)
        {
            if (PauseMenu != null)
            {
                PauseMenu.SetActive(state);
            }
        }

    }
}