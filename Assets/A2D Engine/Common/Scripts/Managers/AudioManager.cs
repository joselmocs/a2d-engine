﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using A2D.Tools;


namespace A2D.Engine
{
    [AddComponentMenu("Adventure2D/Managers/Audio Manager")]
    public class AudioManager : Singleton<AudioManager> {
        public AudioMixer AudioMixer;

        [HideInInspector]
        public List<float> AudioMixerValues = new List<float>{-80, -70, -60, -50, -40, -30, -20, -15, -10, -5, 0};

        public void SetVolume(string mixerGroup, float volume)
        {
            AudioMixer.SetFloat(mixerGroup, AudioMixerValues[Mathf.RoundToInt(volume)]);
        }

        public float GetRawVolume(string mixerGroup)
        {
            float volume;
            AudioMixer.GetFloat(mixerGroup, out volume);
            return volume;
        }

        public float GetVolume(string mixerGroup)
        {
            float volume = GetRawVolume(mixerGroup);
            int indexOfVolume = AudioMixerValues.IndexOf(Mathf.Round(volume));
            if (indexOfVolume == -1) {
                indexOfVolume = 10;
            }

            return (float)indexOfVolume;
        }

    }
}