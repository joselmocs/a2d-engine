﻿using System.IO;
using UnityEngine;


namespace A2D.Engine
{
    public class GameSettings
    {
        protected string _settingsFile;
        public bool fullScreen;
        public int resolutionWidth;
        public int resolutionHeight;
        public int qualityLevel;
        public float masterVolume;
        public float musicVolume;
        public float soundVolume;

        public GameSettings(string settingsFile)
        {
            _settingsFile = settingsFile;
        }

        public void Save()
        {
            File.WriteAllText(_settingsFile, JsonUtility.ToJson(this, true));
            ApplySettings();
        }

        public void Load()
        {
            if (File.Exists(_settingsFile)) {
                GameSettings loadedGameSettings = JsonUtility.FromJson<GameSettings>(File.ReadAllText(_settingsFile));

                // video
                fullScreen = loadedGameSettings.fullScreen;
                resolutionWidth = loadedGameSettings.resolutionWidth;
                resolutionHeight = loadedGameSettings.resolutionHeight;
                qualityLevel = loadedGameSettings.qualityLevel;

                // audio
                masterVolume = loadedGameSettings.masterVolume;
                musicVolume = loadedGameSettings.musicVolume;
                soundVolume = loadedGameSettings.soundVolume;
            }
            else {
                // video
                fullScreen = Screen.fullScreen;
                resolutionWidth = Screen.currentResolution.width;
                resolutionHeight = Screen.currentResolution.height;
                qualityLevel = QualitySettings.GetQualityLevel();

                // audio
                masterVolume = AudioManager.Instance.GetVolume("MasterVolume");
                musicVolume = AudioManager.Instance.GetVolume("MusicVolume");
                soundVolume = AudioManager.Instance.GetVolume("SoundVolume");
            }

            Save();
        }

        public void ApplySettings()
        {
            // audio
            AudioManager.Instance.SetVolume("MasterVolume", masterVolume);
            AudioManager.Instance.SetVolume("MusicVolume", musicVolume);
            AudioManager.Instance.SetVolume("SoundVolume", soundVolume);

            // video
            QualitySettings.SetQualityLevel(qualityLevel);
            Screen.SetResolution(resolutionWidth, resolutionHeight, fullScreen, Screen.currentResolution.refreshRate);
        }
    }
}
